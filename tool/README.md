## Build GCC Toolchain

    # Download GNU Binutils 2.31.1 and uncompress.
    wget http://ftp.gnu.org/gnu/binutils/binutils-2.31.1.tar.xz
    tar xf binutils-2.31.1.tar.xz

    # Download GNU GCC 8.2.0 and uncompress.
    wget http://ftp.gnu.org/gnu/gcc/gcc-8.2.0/gcc-8.2.0.tar.xz
    tar xf gcc-8.2.0.tar.xz

    # Go to gcc folder, download dependencies and create a symbol link to binutils.
    cd gcc-8.2.0
    ./contrib/download_prerequisites
    ln -s ../binutils-2.31.1 binutils

    # Go to binutils folder, apply patch.
    cd binutils
    git apply <pios-repo-root>/tool/patch/binutils.2.31.1.pios.patch

    # Back to gcc folder, apply patch.
    cd ..
    git apply <pios-repo-root>/tool/patch/gcc.8.2.0.pios.patch

    # Create a folder to hold build files.
    cd ..
    mkdir gcc-build
    cd gcc-build

    # Config gcc.    
    ../gcc-8.2.0/configure --target=aarch64-pios --prefix=`pwd`/../aarch64-pios-gcc --disable-nls --disable-multilib --enable-languages=c

    # Build and install.
    make -j`nproc` all-gcc "CFLAGS=-g0 -O3" "CXXFLAGS=-g0 -O3"
    make install all-gcc all-binutils
